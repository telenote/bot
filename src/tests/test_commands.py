import pytest

from commands import TokenCommand, LoginCommand

from mocks import StorageMock


class TestTokenCommand:
    pytestmark = pytest.mark.asyncio

    async def get_command_result(self, payload):
        storage = StorageMock()
        command = TokenCommand(payload, storage)

        return await command.execute()

    async def test_token_command_default(self):
        result = await self.get_command_result('/token')

        assert 'Token command syntax:' in result['message']

    async def test_token_command_help(self):
        result = await self.get_command_result('/token help')

        assert 'Token command syntax:' in result['message']

    async def test_token_command_get(self):
        result = await self.get_command_result('/token get')

        assert 'Current token is `None`' == result['message']

    async def test_token_command_get_with_previosly_setted_value(
        self,
        storage
    ):
        command = TokenCommand('/token get', storage)

        result = await command.execute()

        assert 'Current token is `value`' == result['message']

    async def test_token_command_set(self, mock_validate_user_token_success):
        result = await self.get_command_result('/token set value')

        assert 'A new token value was successfully set' == result['message']

    async def test_token_command_set_too_few_arguments(self):
        result = await self.get_command_result('/token set')

        assert 'Too few arguments' in result['message']

    async def test_token_command_set_validation_failure(
        self,
        mock_validate_user_token_failure
    ):
        result = await self.get_command_result('/token set value')

        assert 'Invalid user token' == result['message']

    async def test_token_command_clear(self, storage):
        command = TokenCommand('/token clear', storage)

        result = await command.execute()

        assert 'Token value was successfully cleared' == result['message']
        assert await storage.get('token') is None


class TestLoginCommand:
    pytestmark = pytest.mark.asyncio

    async def get_command_result(self, payload):
        chat_id = 123
        command = LoginCommand(payload, chat_id)

        return await command.execute()

    async def test_login_command_default(self, mock_get_authorize_url):
        result = await self.get_command_result('/login')

        assert 'Please, follow this [link]' in result['message']

    async def test_login_command_default_fails(
        self,
        mock_get_authorize_url_fails
    ):
        result = await self.get_command_result('/login')

        assert 'Unenable to fetch authorization URL' in result['message']

    async def test_login_command_help(self):
        result = await self.get_command_result('/login help')

        assert 'command signature' in result['message']
