from __future__ import absolute_import

import pytest

from commands import token, login

from requests.exceptions import RequestError

import storages

from mocks import StorageMock


@pytest.fixture
def mock_storage(monkeypatch):
    monkeypatch.setattr(storages, 'Storage', StorageMock)


@pytest.fixture
def mock_validate_user_token_success(monkeypatch):
    async def mock_function(token):
        return True

    monkeypatch.setattr(token, 'validate_user_token', mock_function)


@pytest.fixture
def mock_validate_user_token_failure(monkeypatch):
    async def mock_function(token):
        return False

    monkeypatch.setattr(token, 'validate_user_token', mock_function)


@pytest.fixture
def mock_get_authorize_url(monkeypatch):
    async def mock_function(chat_id):
        return 'https://www.evernote.com/oauth'

    monkeypatch.setattr(login, 'get_authorize_url', mock_function)


@pytest.fixture
def mock_get_authorize_url_fails(monkeypatch):
    async def mock_function(chat_id):
        raise RequestError()

    monkeypatch.setattr(login, 'get_authorize_url', mock_function)


@pytest.fixture
async def storage(mock_storage):
    s = storages.Storage()
    await s.set('token', 'value')
    return s
