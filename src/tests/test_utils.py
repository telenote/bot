import pytest

import utils
from utils import create_note


@pytest.fixture
def mock_send_create_request(monkeypatch):
    async def mock_function(body, token, resourses):
        if resourses:
            body += str(resourses)
        return {
            'title': body,
            'contnet': body
        }

    monkeypatch.setattr(utils, 'send_create_request', mock_function)


class TestCreateNote:
    pytestmark = pytest.mark.asyncio

    async def test_create_note(self, mock_send_create_request):
        body = 'A simple text for note'

        result = await create_note(body, 'token')

        assert 'A note with title' in result['message']

    async def test_create_note_empty_body(self):
        body = ''

        result = await create_note(body, 'token')

        assert 'empty body' in result['message']
