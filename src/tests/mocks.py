import storages


class StorageMock:
    _storage = {}

    async def get(self, key):
        return self._storage.get(key, None)

    async def set(self, key, value):
        self._storage[key] = value
        return value

    async def clear(self, key):
        self._storage[key] = None
        return True
