from enums import ContentType


class ResourceFetcher:
    def __init__(self, message, bot):
        self._message = message
        self._bot = bot
        self.resourses = []

    async def fetch(self):
        self._fetch_images()
        return self.resourses

    async def _fetch_images(self):
        if self.message.photo:
            for image in self.message.photo[1::2]:
                file_url = self._get_file_url(image.file_id)
                resourse = {
                    'content_type': ContentType.IMAGE_JPEG,
                    'url': file_url
                }
                self.resourses.append(resourse)

    async def _get_file_url(self, file_id: int):
        file_data = self._bot.get_file(file_id)

        file_url = 'https://api.telegram.org/file/bot{token}/{file_path}' % {
            'token': settings.TELEGRAM_API_TOKEN,
            'file_path': file_data.file_path
        }

        return file_url


async def get_resources(message, bot):
    fetcher = ResourceFetcher(message, bot)
    resources = await fetcher.fetch()

    return resources
