from enum import Enum


class ContentType(Enum):
    """
    Evernote supported content types
    """

    IMAGE_GIF = 'image/gif'
    IMAGE_JPEG = 'image/jpeg'
    IMAGE_PNG = 'image/png'
    AUDIO_WAV = 'audio/wav'
    AUDIO_MPEG = 'audio/mpeg'
    AUDIO_AMR = 'audio/amr'
    APPLICATION_PDF = 'application/pdf'
