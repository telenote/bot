from api import validate_user_token

from requests.exceptions import RequestError

from storages import Storage

from .exceptions import CommandError
from .base import BaseAsyncCommand


class TokenCommand(BaseAsyncCommand):
    command_signature = '/token'

    def __init__(
            self,
            payload: str,
            storage: Storage,
            command_signature: str = None
    ):
        super().__init__(payload, command_signature)
        self.storage = storage

    async def execute_action_help(self):
        return {'message': (
            'Token command syntax:\r\n\r\n'
            '`/token` or `/token help` -- get this help message;\r\n'
            '`/token get` -- get the current token value;\r\n'
            '`/token set <value>` -- set the current token value;\r\n'
            '`/token clear` -- remove current token value.\r\n\r\n'
        )}

    async def execute_action_get(self):
        token = await self.storage.get('token')
        return {'message': f'Current token is `{token}`'}

    async def execute_action_clear(self):
        await self.storage.clear('token')
        return {'message': 'Token value was successfully cleared'}

    async def execute_action_set(self, token):
        try:
            is_valid = await validate_user_token(token)
        except RequestError:
            raise CommandError(
                'Unenable to validate your token. Please, try again later. '
                'If the problem would not resolve -- contact administrator.'
            )

        if not is_valid:
            raise CommandError('Invalid user token')

        await self.storage.set('token', token)
        return {'message': 'A new token value was successfully set'}
