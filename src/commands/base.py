from typing import Dict, Any, Callable, List, Optional

from .exceptions import CommandError


class BaseAsyncCommand:
    """
    This is abstract base class for asynchronius commands. It provides
    API to execute different scripts based on given payload.

    Attrivutes:
        command_signature (str): A string command representation. For
            example, `/token` or `/login`

    Args:
        payload (str): A command text (usually, the telegram message text).
        command_signature (Optional[str]): Rewrites the default command
            signature.
    """
    command_signature: str = None

    def __init__(self, payload: str, command_signature: Optional[str] = None):
        self.payload = payload

        if command_signature:
            self.command_signature = command_signature

    async def execute(self) -> Dict[str, str]:
        """
        Executes the appropriate script (action) based on command payload.

        Returns:
            Dict[str, str]: {"message": ...}
        """

        try:
            command_arguments = self._parse_command_arguments()

            action = command_arguments['action']
            arguments = command_arguments.get('arguments')

            action_method = self._get_action_method(action)
            return await self._execute_action_method(action_method, arguments)
        except CommandError as error:
            return {'message': str(error)}

    async def execute_action_default(self):
        """
        Executed script by default (usually, it returns the help text).

        Handles `/<command>` usage.
        """
        return await self.execute_action_help()

    async def execute_action_help(self):
        """
        Handles `/<command> help` script
        """
        raise NotImplementedError()

    def get_command_signature(self) -> str:
        """
        Gets the current command signature if its set.
        """
        if not self.command_signature:
            raise NotImplementedError()
        return self.command_signature

    def _parse_command_arguments(self):
        """
        Gets the current action and arguments from command payload.
        """
        signature = self.get_command_signature()
        command_without_signature = self.payload.replace(signature, '')

        if not command_without_signature:
            return {'action': 'default'}

        command_components = command_without_signature.split()
        action = command_components[0]
        return {'action': action, 'arguments': command_components[1:]}

    def _get_action_method(
        self,
        action: str
    ) -> Callable[..., Dict[str, Any]]:
        """
        Gets the appropriate object method for current action

        Raises:
            CommandError: If the appropriate object method was not found
        """

        action_method = getattr(self, f'execute_action_{action}')

        if not action_method:
            raise CommandError('Invalid action method')

        return action_method

    async def _execute_action_method(
        self,
        action_method: Callable[..., Dict[str, Any]],
        arguments: Optional[List[Any]] = None
    ) -> Dict[str, Any]:
        """
        Executes the appropriate object method for current action
        with given arguments.
        """

        try:
            if not arguments:
                return await action_method()
            return await action_method(*arguments)
        except TypeError:
            signature = self.get_command_signature()
            raise CommandError(
                f'Too few arguments! See `{signature} help` for details'
            )
