from api import get_authorize_url

from requests.exceptions import RequestError

from .base import BaseAsyncCommand
from .exceptions import CommandError


class LoginCommand(BaseAsyncCommand):
    command_signature = '/login'

    def __init__(self, payload, chat_id, command_signature=None):
        super().__init__(payload, command_signature)
        self.chat_id = chat_id

    async def execute_action_default(self):
        try:
            authorize_url = await get_authorize_url(self.chat_id)
        except RequestError:
            raise CommandError(
                'Unenable to fetch authorization URL. Please, try again '
                'later or contact administrator if the problem would not '
                'resolve.'
            )

        return {'message': (
            f'Please, follow this [link]({authorize_url}) to authorize '
            'this bot in your Evernote account, so that bot can create new '
            'notes for you.'
        )}

    async def execute_action_help(self):
        return {'message': (
            'A `/login` command signature:\r\n\r\n'
            '  `/login` -- get the login (authorization) URL;\r\n'
            '  `/login help` -- get the message text;\r\n'
        )}
