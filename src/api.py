"""
This module contains functions to work with Telenote API (it's backend
part).
"""

import settings

from requests import send_request


async def get_authorize_url(chat_id):
    """
    Retrieves the Evernote OAuth Authorize URL.

    Args:
        chat_id (int): Current user with bot chat unique ID.

    Returns:
        str: Authorize URL
    """
    request_url = f'{settings.API_URL}/oauth/authorize-url/'
    response = await send_request(
        request_url,
        method='get',
        json={
            'chat_id': chat_id
        }
    )

    return response['authorize_url']


async def create_note(body: str, token: str, resourses=None) -> dict:
    """
    Sends request t ocreate a new user note.

    Args:
        body (str): A note body to be created.
        token (str): A unique user access token retrieved from Evernote
            API.

    Returns:
        dict: Fetched data from API
    """

    request_url = f'{settings.API_URL}/notes/'

    return await send_request(
        request_url,
        method='post',
        json={
            'body': body,
            'resourses': resourses
        },
        headers={
            'Authorization': f'Token {token}'
        }
    )


async def validate_user_token(token: str) -> bool:
    """
    Validated user access token value (checks if it is presented in
    database). This function does not actually access database, but
    sends request to an API that checks it and responses with
    validation resuls.
    """

    request_url = settings.API_URL + '/tokens/validate/'
    response = await send_request(
        request_url,
        method='post',
        json={
            'access_token': token
        }
    )

    return response['is_valid']
