class RequestError(Exception):
    """
    This error raises on invalid HTTP request or on request sending failure.
    """
    pass
