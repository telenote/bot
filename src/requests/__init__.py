import aiohttp
from aiohttp.client_exceptions import ClientConnectionError

from requests.exceptions import RequestError


_allowed_http_methods = ('get', 'post', 'put', 'patch', 'delete')


async def send_request(url: str, method: str = 'get', **kwargs) -> dict:
    """
    Sends HTTP request and retrieves data from it's response.

    Args:
        url (str): A URL that request should be sent to.
        method (str): A HTTP method string representation (lowercase!)
        **kwargs (dict): A list of request properties, i.e. headers,
            body etc.

    Raises:
        RequestError: On request sending erorr

    Returns:
        dict: Deserializes JSON response data.
    """

    async with aiohttp.ClientSession() as session:
        method = method.lower()
        if method not in _allowed_http_methods:
            raise RequestError(
                f'Unsupported HTTP method {method}, must be one of '
                + _allowed_http_methods
            )

        request_function = getattr(session, method)

        try:
            response = await request_function(url, **kwargs)
        except ClientConnectionError:
            raise RequestError(
                'Lost connection to server. Please, try again later. If the '
                'problem would not reusolve -- contact administrator.'
            )

        if response.status >= 300:
            raise RequestError(
                f'Server responded with HTTP status {response.status}'
            )

        return await response.json()
