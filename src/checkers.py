"""
A utility functions to check the message handle condictions.
"""

from aiogram.types import Message


def check_message_forwarded_from_channel(message: Message) -> bool:
    """
    Checks if the given message was forwarded from another chat (channel).
    """
    return message.forward_from_chat is not None
