"""
A main bot module, contains bot and dispatcher definitions.
"""
import logging

from aiogram import Bot, Dispatcher, types, executor
from aiogram.contrib.fsm_storage.redis import RedisStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State
from aiogram.types.message import ContentType

from checkers import check_message_forwarded_from_channel

from commands import TokenCommand, LoginCommand
from commands.exceptions import CommandError

from decorators import token_required

from resources import get_resources

import settings

from storages import Storage

from utils import create_note


# Configure bot logging
logging.basicConfig(level=logging.INFO)


# Define the chat's state storage
storage = RedisStorage(settings.REDIS_HOST, 6379, 1)


# Create a telegram bot by given TOKEN
bot = Bot(token=settings.TELEGRAM_API_TOKEN)


# Dispatcher is an abstraction that handles bot's events
dispatcher = Dispatcher(bot, storage=storage)


# A state that stores each user Access Token
# Without this token user can not create notes via Telenote Bot
token = State()


@dispatcher.message_handler(commands='start')
async def send_welcome(message: types.Message):
    """
    Sends some refernce information about the bot usage.

    Handles:
        /start
    """

    await message.reply(
        '**Telenote Bot welcomes!**\r\n\r\n'
        'Before you get start with Bot please use the `/login` command to '
        'authorize bot application in your Evernote profile. Copy the '
        'retrieved token command (e.g. `/token <value>`) and paste it to '
        'this chat in order to save the obtained token in application so '
        'that a bot can use it to create a notes for you. Once you have '
        'done you can forward any message from a third-patry chats to make '
        'the bot save them into your Evernote profile.\r\n\r\n'
        'Use /help if you are lost\r\n\r\n'
        'Visit our website if you still want some more: http://telenote.tk/',
        parse_mode='Markdown',
        reply=False
    )


@dispatcher.message_handler(commands='help')
async def send_help_message(message: types.Message):
    """
    Sends the list of commands to user.

    Handles:
        /help
    """

    await message.reply(
        'List of commands (use `/<command> help`, e.g. `/login help` to get '
        'more details about a particular command):\r\n\r\n'
        '`/start` -- repeat the brief beginner\'s guide;\r\n'
        '`/login` -- log in your Evernote profile to be able to create '
        'notes;\r\n'
        '`/token` -- manage Evernote access token data (use `/token help` '
        'to get more details).',
        parse_mode='Markdown',
        reply=False
    )


@dispatcher.message_handler(commands='login')
async def send_authorize_url(message: types.Message):
    """
    Sends user the Evernote API authorization link

    Handles:
        /login
        /login help
    """

    command = LoginCommand(message.text, message.chat.id)

    try:
        result = await command.execute()
    except CommandError as e:
        await message.reply(str(e), parse_mode='Markdown')
        return

    await message.reply(result['message'], parse_mode='Markdown')


@dispatcher.message_handler(commands='token', state='*')
async def obtain_user_token(message: types.Message, state: FSMContext):
    """
    An API for interact with Evernotes Access Token.

    Handles:
        /token
        /token help
        /token get
        /token set
        /token clear
    """

    storage = Storage(state)
    command = TokenCommand(message.text, storage)

    try:
        command_result = await command.execute()
    except CommandError as e:
        await message.reply(str(e), parse_mode='Markdown')
        return

    await message.reply(command_result['message'], parse_mode='Markdown')


@dispatcher.message_handler(
    check_message_forwarded_from_channel,
    state='*',
    content_types=(
        ContentType.TEXT,
        ContentType.PHOTO,
        ContentType.AUDIO,
        ContentType.VIDEO,
    )
)
@token_required
async def handle_note_creation(
    message: types.Message,
    token: str
):
    """
    Creates a new note for user based on forwarded from other channel
    message. Requires access token to be set by user.

    Handles:
        forwarded message
        forwarded video -- saves only caption
        forwarded audio -- saves only caption
        forwarded photo -- saves only caption

    TODO:
        Implement media saving (video, audio, photo etc).
        Implement MediaGroups handling.
    """

    resourses = await get_resources(message, bot)

    body = message.text or message.caption
    result = await create_note(body, token, resourses)
    await message.reply(result['message'], parse_mode='Markdown')


async def on_shutdown(dispatcher):
    """
    Called on bot shutdown. Closes the bot storage.
    """

    await dispatcher.storage.close()
    await dispatcher.storage.wait_closed()


if __name__ == '__main__':
    executor.start_polling(
        dispatcher,
        skip_updates=True,
        on_shutdown=on_shutdown
    )
