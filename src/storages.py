from typing import Any

from aiogram.dispatcher import FSMContext


class Storage:
    def __init__(self, state: FSMContext):
        self.state = state

    async def get(self, key: str) -> Any:
        async with self.state.proxy() as data:
            return data.get(key, None)

    async def set(self, key: str, value: Any) -> Any:
        async with self.state.proxy() as data:
            data[key] = value
            return value

    async def clear(self, key: str) -> bool:
        async with self.state.proxy() as data:
            data[key] = None
            return True
