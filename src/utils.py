import hashlib
import io

from typing import Dict, List, Optional

from api import create_note as send_create_request

import settings

from requests import send_request
from requests.exceptions import RequestError


async def create_note(
    body: str,
    token: str,
    resourses: Optional[List[Dict[str, str]]] = None
) -> Dict[str, str]:
    """
    A create_note API function wrapper for handling the note creation
    by user.

    Args:
        body (str): A note body to be created
        token (str): An Evernote OAuth user token that belongs to user
            who is created note owner.

    Returns:
        Dict[str, str]: {'message': <message text for user>}
    """

    if not body:
        return {
            'message': 'It is not allowed to create notes with empty body'
        }

    try:
        response = await send_create_request(body, token, resourses)
    except RequestError as e:
        return {'message': str(e)}

    title = response.get('title', None)

    if not title:
        return {'message': (
            'Unfortunately, something went wrong. Please, try again later or '
            'contact administrator if the problem would not be resolved.'
        )}

    return {
        'message': f'A note with title **"{title}"** was successfully created'
    }
